package test_exercise;

import java.util.Scanner;

public class MultipleInput {

	public static void main(String[] args) {
		// scanner for multiple input
		
		System.out.println(" Give A value");
		
		Scanner scn=new Scanner(System.in);
		int A = scn.nextInt();

		System.out.println("Give B value");
		int B = scn.nextInt();

		System.out.println("The multiple of " + A + " and " + B + " is " + A * B);
	}

}
